import pandas as pd
from dvclive import Live


df = pd.read_csv("creditcard_2023.csv")

df_agg = df.groupby(by="Class", as_index=False).mean("Amount")

df_agg.to_csv("creditcard_summary.csv")

with Live(save_dvc_exp=False, report=None) as live:
    live.log_metric("class0_mean_amount", df_agg[df_agg["Class"] == 0]["Amount"].values[0])
    live.log_metric("class1_mean_amount", df_agg[df_agg["Class"] == 1]["Amount"].values[0])